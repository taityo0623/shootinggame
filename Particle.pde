class Particle {
  PVector c;
  int r;
  boolean life;

  Particle() {
    c = new PVector();
  }

  PVector getCoordinates() {
    PVector tmp = new PVector(c.x, c.y, c.z);
    return tmp;
  }
  int getRadius() { 
    return r;
  }

  boolean checkCollision(PVector targetCoodinates, int targetRadius) {
    float d = c.dist(targetCoodinates);
    if (d <= r + targetRadius)return true;
    return false;
  }

  boolean checkLife() {
    return life;
  }
  void kill() {
    life = false;
  }

  boolean checkOut() {
    if (c.x > 500 || c.x < 0 || c.y > 500 || c.y < 0)return true;
    return false;
  }
}

