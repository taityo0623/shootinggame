class Wall {
  PVector c;
  PVector wall;
  int w;

  Wall(float x, float y, float l, float a) {
    c = new PVector(x, y);
    wall = new PVector(l * cos(a), l * sin(a));
  }

  Wall(PVector c, PVector wall) { 
    this.c = new PVector();
    this.wall = new PVector();
    this.c = c.get();  
    this.wall = wall.get();
  }

  void draw() {
    stroke(170, 0, 23);
    strokeWeight(5);
    line(c.x, c.y, c.x + wall.x, c.y + wall.y);
    noStroke();
  }
}

