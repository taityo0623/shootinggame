class Player extends Particle {
  float rad;
  int coolTime; 
  ArrayList<Bullet> bulletList;

  Player (PVector c, ArrayList<Bullet> bulletList) {
    c.set(c);
    r = 5;
    life = true;
    this.bulletList = bulletList;
    rad = 0;
    coolTime = 0;
  }

  Player (float x, float y, ArrayList<Bullet> bulletList) {
    c.set(x, y);
    r = 5;
    life = true;
    this.bulletList = bulletList;
    rad = 0;
    coolTime = 0;
  }

  void update() {
    move();
    shoot();
  }

  void draw() {
    fill(0);
    ellipse(c.x, c.y, r * 2, r * 2);
  }

  void move() {
    rad = atan2(mouseY - c.y, mouseX - c.x);
    c.x = mouseX;
    c.y = mouseY;
  }

  void shoot() {
    if (coolTime >= 1) coolTime -= 1;
    if (mousePressed && coolTime <= 0 ) {
      bulletList.add(new Bullet3(c.x + 5 * cos(rad), c.y + 5 * sin(rad), new PVector(3 * cos(rad), 3 * sin(rad), 0)));
      coolTime += 0;
    }
  }
}

