class Bullet2 extends Bullet {
  float ang;
  float dir;
  PVector V;
  float w;
  PImage img;

  Bullet2(float x, float y, PVector v) {
    c.x = x;
    c.y = y;
    c.z = 0;
    r = 8;
    life=true;
    this.v = v;
    ang = 1.20;
    w = 1.0;
    dir = atan2(v.y, v.x);
    V = new PVector(0, 0, 0);
    img = loadImage("Bullet_1.png");
  }

  void move() {
    ang += 0.3;
    if(w < 5.0)w += 0.05;
    V.set(v);
    V.add(-w * sin(ang) * sin(dir), w * sin(ang) * cos(dir), 0);
    c.add(V);
  }

  void draw() {
    image(img, c.x - r, c.y - r);
  }
}

