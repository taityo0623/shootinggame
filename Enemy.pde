class Enemy extends Particle {
  PVector v;

  Enemy(float x, float y, PVector v) {
    c.x = x;
    c.y = y;
    c.z = 0;
    r = 15;
    life=true;
    this.v = v;
  }

  void update() {
    move();
  }

  void move() {
    c.add(v);
  }

  void draw() {
    fill(0, 100, 200, 64);
    ellipse(c.x, c.y, r * 2, r * 2);
  }
}

