Stage stage;

void setup() {
  size(500, 500);
  frameRate(60);
  ellipseMode(CENTER);
  noStroke();
  stage = new Stage();
}

void draw() {
  stage.update();
}
