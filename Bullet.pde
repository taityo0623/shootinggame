class Bullet extends Particle {
  PVector v;

  Bullet() {
  }
  Bullet(float x, float y, PVector v) {
    c.x = x;
    c.y = y;
    c.z = 0;
    r = 3;
    life=true;
    this.v = v;
  }

  void update() {
    move();
  }

  void move() {
    c.add(v);
  }

  void draw() {
    fill(100, 200, 0);
    ellipse(c.x, c.y, r * 2, r * 2);
  }
}

