class Stage {
  Player player;
  ArrayList<Bullet> bulletList;
  ArrayList<Enemy> enemyList;
  int time;
  float fps;

  Stage() {
    PVector tmp = new PVector(0, 0, 0);
    bulletList = new ArrayList<Bullet>();
    enemyList = new ArrayList<Enemy>();
    player = new Player(tmp, bulletList);
    time = 0;
    fps = frameRate;
  }

  void update() {
    player.update(); 
    tmpAddEnemy();

    for (Bullet bullet : bulletList) {
      if (bullet.checkOut())bullet.kill();
      else bullet.update();
    }
    for (Enemy enemy : enemyList) {
      if (enemy.checkOut())enemy.kill();
      else enemy.update();
    }

    checkPBtoECollision();

    removeBullet();
    removeEnemy();

    draw();
  }

  void removeBullet () {
    for (int i = bulletList.size () - 1; i >= 0; i--) {
      if (!bulletList.get(i).life)bulletList.remove(i);
    }
  }
  void removeEnemy () {
    for (int i = enemyList.size () - 1; i >= 0; i--) {
      if (!enemyList.get(i).life)enemyList.remove(i);
    }
  }

  void checkPBtoECollision () {
    for (Bullet bullet : bulletList) {
      for (Enemy enemy : enemyList) {
        if (bullet.checkCollision(enemy.getCoordinates(), enemy.getRadius())) {
          bullet.kill();
          enemy.kill();
        }
      }
    }
  }

  void tmpAddEnemy () {
    float rnd = random(0, 1); 
    if (rnd >= 0.5) {
      Enemy tmpe = new Enemy(500, random(500), new PVector(random(-3, -1), random(-0.5, 0.5), 0)); 
      enemyList.add(tmpe);
    }
  }

  void draw() {
    background(255);
    player.draw();

    for (Bullet bullet : bulletList) {
      bullet.draw();
    }
    for (Enemy enemy : enemyList) {
      enemy.draw();
    }

    information();
  }

  void information() {
    fill(0);
    textSize(12);
    text("time : " + time, 5, 15);
    time++;
    fps=frameRate;
    text("fps : " + fps, 5, 30);
    text("bulletN : " + bulletList.size(), 5, 45);
    text("enemyN : " + enemyList.size(), 5, 60);
  }
}

