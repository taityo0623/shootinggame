class Bullet3 extends Bullet {
  Bullet3(float x, float y, PVector v) {
    super(x, y, v);
  }

  void move() {
    if (c.x > 490)reflect(new PVector(0, 1));
    if (c.x < 10)reflect(new PVector(0, 1));
    if (c.y > 490)reflect(new PVector(1, 0));
    if (c.y < 10)reflect(new PVector(1, 0));
    c.add(v);
  }

  void reflect(PVector wall) {
    wall.normalize();
    float nx = (sq(wall.x) - sq(wall.y)) * v.x + 2 * wall.x * wall.y * v.y;
    float ny = 2 * wall.x * wall.y * v.x - (sq(wall.x) - sq(wall.y)) * v.y;
    v.x = nx;
    v.y = ny;
  }
}

